from kfp import dsl
import json
from typing import NamedTuple
import uuid


@dsl.component(base_image='image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0:v1.0.0',
               target_image='image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.7')
def sample_component1(input1: str, input2: int, mnt_path: str) -> NamedTuple('outputs', output1=str, output2=int):
    """
    JSON 형식으로 된 input1 파일에서 수를 읽어서 input2의 값과 더하여 그 결과 값을 동일한 JSON 형식으로 output1 파일에 저장한다.
    output2에는 결과 값을 저장한다.
    input1과 output1 파일의 형식은 다음과 같다.
     {"input": 231}
    :param input1: input file path
    :param input2: integer
    :param mnt_path: persistent volume의 mount path
    :return: NamedTuple('outputs', output1=str, output2=int)
      output1: output file path
      output2: integer
    """

    output_parent = mnt_path
    output_path = output_parent + f'/output-{uuid.uuid4()}.json'

    with open(input1, 'r') as input1_file:
        data = json.load(input1_file)
    data['input'] = data['input'] + input2
    output2 = data['input']

    with open(output_path, 'w') as o_file:
        json.dump(data, o_file)

    outputs = NamedTuple('outputs', output1=str, output2=int)
    return outputs(output_path, output2)
