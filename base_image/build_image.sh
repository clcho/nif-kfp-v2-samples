#!/bin/bash -e
image_name=image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0
image_tag=v1.0.0
full_image_name=${image_name}:${image_tag}

cd "$(dirname "$0")"
docker build --network=host --no-cache --force-rm -t "${full_image_name}" .
docker push "$full_image_name"

# Output the strict image name, which contains the sha256 image digest
docker inspect --format="{{index .RepoDigests 0}}" "${full_image_name}"
