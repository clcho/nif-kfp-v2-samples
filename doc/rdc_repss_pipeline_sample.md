# NIF 데이터 수집 및 사전 처리 파이프라인 샘플

### rdc_repss_sample_pipeline 구성

- rdc_collection KFP 컴포넌트와 repss_component KFP 컴포넌트로 구성
- 소스 구조
    - /components/nif/rd_collection_v1.0.1.yaml
    - /components/nif/repss_component.yaml
    - /pipelines/rdc_repss_pipeline_v1.0.1.py  (주의: default repss_order_id는 testbed1에서만 가능)
    - /pipelines/rdc_repss_pipeline_tb2_v1.0.1.py (주의: default repss_order_id는 testbed2에서만 가능)

### 파이프라인 코드 컴파일 (파이프라인 yaml 파일 생성)

- KFP 개발 환경 구성 
  - [NIF 테스트베드에서 KFP SDK v2 기반 컴포넌트 및 파이프라인 개발, 설치, 실행하는 절차와 방법](guide.md) 참조
- KFP 개발 환경에서 파이프라인 컴파일
    
    ```bash
    python rdc_repss_pipeline.py
    ```
    
- 생성된 rdc_repss_pipeline.yaml 파일을 로컬 머신으로 다운로드 (scp 또는 sftp 등을 이용)

### 파이프라인 yaml 파일 만들 때 주의 사항

- 파이프라인을 구성할 컴포넌트 버전 확인
    - 컴포넌트의 yaml 파일 내의 컴포넌트 버전이 harbor에 올라와 있는 최신 버전과 일치하는지 확인
- 파이프라인 py 파일 확인
    - 올바른 버전의 컴포넌트 yaml 파일을 참조하는지 확인
- 파이프라인을 실행했을 때 특정한 에러 메시지 없이 특정 컴포넌트가 지나치게 오랫동안 running 상태로 계속 남아있는 경우
    - 해당 컴포넌트의 버전이 harbor image registry에 있는지 확인해 볼 것 (일치하는 버전의 이미지가 없는 경우 이런 현상이 생김)
    - Run parameters 값이 정확한지 다시 확인할 것
      - pvc_name에 적은 이름의 volume이 존재하는지 확인
      - pipeline_input1에 적은 파일의 경로와 이름이 정확한지 확인

### KFP Web UI에서 파이프라인 생성 및 Run 실행

- [NIF 테스트베드에서 KFP SDK v2 기반 컴포넌트 및 파이프라인 개발, 설치, 실행하는 절차와 방법](guide.md)의 파이프라인 시험 참조
    - 과정 중 Persistent Volume 생성 과정과 Notebook 생성 및 파이프라인 입력 파일 생성 과정은 옵션 사항임. 이미 생성되어 있는 Persistent Volume 및 notebook이 있다면 이를 그대로 활용하면 됨.

### 데이터 수집 KFP 컴포넌트의 입력 파라미터 설명

- mnt_path
    - persistent volume에 대한 mount path. 디폴트 값 (`/data`)을 그대로 사용해야 함
- pvc_name
    - 데이터 수집 요청 yaml 파일이 저장된 persistent volume 이름
- rdc_order_path
    - 데이터 수집 요청 yaml 파일 경로. 지정한 mnt_path 값으로 시작해야 함.
    - 데이터 수집 요청 yaml 파일 포맷
        
        ```xml
        version: 0.1.0
        odmss:
          ip: 10.3.30.22
          port: 8180
          # kfp 컴포넌트가 주기적으로 데이터 수집 상태를 확인하는 주기 (단위: 분)
          rd_status_check_interval: 1 
        component_params:
          collection_time_spec_type: SPECIFY_DURATION
          # 수집 기간
          collection_duration: 5 
          # 수집 기간 단위: MINUTE, HOUR, DAY
          collection_duration_unit: HOUR 
          # 수집 시작 전 지연 시간, 단위: 분 (kfp 컴포넌트가 시작된 후 1분 후에 수집 시작하도록 설정)
          delay_to_start: 1
        collection_request:
          user_id: user001
          data_src_id: 416050e4-4f67-4c12-936b-010e0a3d9722
          # collection_time_spec_type이 SPECIFY_DURATION의 경우 다음 시간 파라미터를 비워둠
          collection_start_req_time: ""
          collection_end_req_time: ""
          # 필터링 규칙, tcpdump 필터 포맷 사용
          collection_filter: " " 
          # 수집할 페이로드 크기
          collection_payload_size: 1480 
          # 플로우 단위시간 원시 데이터의 단위시간
          collection_interval: 60 
          callback_url: "http://localhost:10000"
          # 수집 대상 그룹
          target_attr_groups: 
            - FLOW__ALL
            - FLOW_DELTA__ALL
            - FLOW_PACKET__ALL
          target_pods:
            - name: "target_pod_1"
              namespace: "target_pod_1_namespace"
            - name: "target_pod_2"
              namespace: "target_pod_2_namespace"
        
        ```

### REPSS KFP 컴포넌트의 입력 파라미터 설명

- repss_method
    - 디폴트 값(`POST`)을 그대로 사용하면 됨
- repss_order_id
    - REPSS에 사전 등록한 주문서 아이디
- repss_url
    - 주문 URL. 디폴트 값(http://10.3.30.27:18090/repss/v2.0.0/orders)을 그대로 사용하면 됨