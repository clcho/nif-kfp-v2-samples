from kfp import dsl
from kfp import components
from kfp import compiler
from typing import NamedTuple
from kfp import kubernetes

# pipeline에서 사용할 component yaml 파일을 읽어서 component 객체를 생성함
sample_comp1 = components.load_component_from_file(
    '../components/sample_component1/src/component_metadata/sample_component1.yaml')

@dsl.pipeline
def sample_pipeline_1(
        pipeline_input1: str="/data/sample1_input.json",
        pipeline_input2: int=1,
        pvc_name: str="sample-volume",
        mnt_path: str="/data") -> NamedTuple('pipeline_outputs', output1=str, output2=str, output3=int, output4=int):
    """
    파일 path로 지정된 파라미터를 처리하는 방법을 보여주는 샘플 파이프라인.
    sample_component1 컴포넌트 두 개를 연결하여 하나의 파이프라인으로 구성함.
    sample_component1 컴포넌트는 JSON 형식의 입력 파일에 있는 값을 읽어서 두 번째 입력 파라미터 값과 더하여 출력 파일을 만듬.

    :param pipeline_input1: JSON 형식의 내용을 저장한 파일 path. 첫 번째 컴포넌트의 입력 파라미터 값으로 들어감
    :param pipeline_input2: integer
    :param pvc_name: persistent volume 이름
    :return:
    """
    task1 = sample_comp1(input1=pipeline_input1, input2=pipeline_input2, mnt_path=mnt_path)
    # component task의 caching option default 값이 True여서 task를 반복 실행할 때 task의 입력에 변화가 없으면
    # cache에서 바로 출력값을 가져옴 (즉, 실제 실행하지 않음)
    # 이를 방지하려면 caching option을 False로 설정해야 함
    task1.set_caching_options(False)
    task2 = sample_comp1(input1=task1.outputs['output1'], input2=pipeline_input2, mnt_path=mnt_path)

    # 아래에서 mount_path 값으로 파이프라인 파라미터로 받은 mnt_path를 사용하면 에러 발생함.
    # 아직 파이프라인 입력 파라미터로 받은 mnt_path를 사용하여 아래 mount_path를 지정하는 방법을 모름.
    # 이 이슈가 해결되기 전까지는 파이프라인 런을 생성할 때 mnt_path는 "/data" 값을 그대로 사용할 것.
    # kubernetes.mount_pvc(task1, pvc_name=pvc_name, mount_path=mnt_path)
    # kubernetes.mount_pvc(task2, pvc_name=pvc_name, mount_path=mnt_path)
    kubernetes.mount_pvc(task1, pvc_name=pvc_name, mount_path='/data')
    kubernetes.mount_pvc(task2, pvc_name=pvc_name, mount_path='/data')
    task2.after(task1)

    p_outputs = NamedTuple('pipeline_outputs', output1=str, output2=str, output3=int, output4=int)

    return p_outputs(
        task1.outputs['output1'],
        task2.outputs['output1'],
        task1.outputs['output2'],
        task2.outputs['output2'])


compiler.Compiler().compile(sample_pipeline_1, 'sample_pipeline1.yaml')
