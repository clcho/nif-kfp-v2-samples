from kfp import dsl
from kfp import components
from kfp import compiler
from kfp import kubernetes

from typing import NamedTuple

rd_collection = components.load_component_from_file(
    '../components/nif/rd_collection_v1.0.1.yaml')
handle_result = components.load_component_from_file(
    '../components/nif/handle_collection_result_v1.0.0.yaml')

@dsl.pipeline
def rd_collection_pipeline(
        pipeline_input1: str="/data/input_specify_duration.yaml",
        pvc_name: str="nif-volume",
        mnt_path: str="/data") -> NamedTuple('pipeline_outputs', output1=str, output2=str, output3=str, output4=str, output5=str):
    """
    파일경로로 지정된 입력 파라미터를 기반으로 수집주문 요청을 실행하는 컴포넌트와 그 결과를 처리하는 파이프라인 예시.
    rd_collection 컴포넌트와 handle_result 컴포넌트 두 개를 연결하여 하나의 파이프라인으로 구성함.
    rd_collection 컴포넌트는 YAML 형식의 입력 파일에 있는 값을 읽어서 ODMC 로 수집주문 요청을 수행하고
    그 결과를 JSON 형태의 출력 파일을 만듬.

    :param pipeline_input1: YAML 형식의 내용을 저장한 파일 path. 첫 번째 컴포넌트의 입력 파라미터 값으로 들어감
    :param pvc_name: persistent volume 이름
    :param mnt_path: persistent volume에 대한 mount path
    :return:
    """
    task1 = rd_collection(input1=pipeline_input1, mnt_path=mnt_path)
    task1.set_caching_options(False)
    kubernetes.mount_pvc(task1, pvc_name=pvc_name, mount_path='/data')

    task2 = handle_result(input1=task1.outputs['output1'], input2=task1.outputs['output2'])
    task2.set_caching_options(False)
    kubernetes.mount_pvc(task2, pvc_name=pvc_name, mount_path='/data')
    task2.after(task1)

    o1, o2, o3 = task1.outputs['output1'], task1.outputs['output2'], task1.outputs['rdc_result']
    o4, o5 = task2.outputs['output1'], task2.outputs['output2']

    p_outputs = NamedTuple('pipeline_outputs', output1=str, output2=str, output3=str, output4=str, output5=str)

    return p_outputs(o1, o2, o3, o4, o5)


compiler.Compiler().compile(rd_collection_pipeline, __file__.replace('.py', '.yaml'))
