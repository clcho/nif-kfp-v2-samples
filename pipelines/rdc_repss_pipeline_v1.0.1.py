from kfp import dsl
from kfp import components
from kfp import compiler
from kfp import kubernetes

from typing import NamedTuple

rd_collection = components.load_component_from_file(
    '../components/nif/rd_collection_v1.0.1.yaml')
repss_app = components.load_component_from_file(
    '../components/nif/repss_component.yaml')
@dsl.pipeline
def rdc_repss_pipeline(
        pvc_name: str="nif-volume",
        mnt_path: str="/data",
        rdc_order_path: str="/data/input_specify_duration.yaml",
        # repss_order_id: str="430b8f2c-076f-4aa1-8c73-4d7f8b312b56",  # for testbed2, for FLOW data ids
        repss_order_id: str = "b3760387-a4c0-4803-9f5b-cffb0834ecc4",  # for testbed1, for FLOW, FLOW_DELTA, FLOW_PACKET data ids
        repss_url: str = "http://10.3.30.27:18090/repss/v2.0.0/orders",
        repss_method: str = "POST"
) -> NamedTuple('pipeline_outputs', rdc_result=str, repss_output=str):
    """
    NIF 데이터 수집 및 데이터 처리 파이프라인

    :param pvc_name: persistent volume 이름
    :param mnt_path: persistent volume에 대한 mount path
    :param rdc_order_path: 데이터 수집 주문서 file path
    :param repss_order_id: REPSS 처리 주문서 order id
    :param repss_url: REPSS 처리 주문 url
    :param repss_method: REPSS 처리 주문 method

    :return:
    """

    task1 = rd_collection(input1=rdc_order_path, mnt_path=mnt_path)
    task1.set_caching_options(False)
    # 아래에서 mount_path 값으로 파이프라인 파라미터로 받은 mnt_path를 사용하면 에러 발생함.
    # 아직 파이프라인 입력 파라미터로 받은 mnt_path를 사용하여 아래 mount_path를 지정하는 방법을 모름.
    # 이 이슈가 해결되기 전까지는 파이프라인 런을 생성할 때 mnt_path는 "/data" 값을 그대로 사용할 것.
    # kubernetes.mount_pvc(task1, pvc_name=pvc_name, mount_path=mnt_path)
    kubernetes.mount_pvc(task1, pvc_name=pvc_name, mount_path='/data')
    p_o1 = task1.outputs['rdc_result']

    task2 = repss_app(orderid=repss_order_id, dataids=task1.outputs['output2'], url=repss_url, method=repss_method)
    task2.set_caching_options(False)
    task2.after(task1)
    p_o2 = task2.outputs['output']

    p_outputs = NamedTuple('pipeline_outputs', rdc_result=str, repss_output=str)

    return p_outputs(p_o1, p_o2)


compiler.Compiler().compile(rdc_repss_pipeline, __file__.replace('.py', '.yaml'))
