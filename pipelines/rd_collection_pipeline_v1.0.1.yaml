# PIPELINE DEFINITION
# Name: rd-collection-pipeline
# Description: 파일경로로 지정된 입력 파라미터를 기반으로 수집주문 요청을 실행하는 컴포넌트와 그 결과를 처리하는 파이프라인 예시.
#              rd_collection 컴포넌트와 handle_result 컴포넌트 두 개를 연결하여 하나의 파이프라인으로 구성함.
#              rd_collection 컴포넌트는 YAML 형식의 입력 파일에 있는 값을 읽어서 ODMC 로 수집주문 요청을 수행하고
#              그 결과를 JSON 형태의 출력 파일을 만듬.
# Inputs:
#    mnt_path: str [Default: '/data']
#    pipeline_input1: str [Default: '/data/input_specify_duration.yaml']
#    pvc_name: str [Default: 'nif-volume']
# Outputs:
#    output1: str
#    output2: str
#    output3: str
#    output4: str
#    output5: str
components:
  comp-handle-collection-result:
    executorLabel: exec-handle-collection-result
    inputDefinitions:
      parameters:
        input1:
          parameterType: STRING
        input2:
          parameterType: STRING
    outputDefinitions:
      parameters:
        output1:
          parameterType: STRING
        output2:
          parameterType: STRING
  comp-rd-collection:
    executorLabel: exec-rd-collection
    inputDefinitions:
      parameters:
        input1:
          parameterType: STRING
        mnt_path:
          parameterType: STRING
    outputDefinitions:
      parameters:
        output1:
          parameterType: STRING
        output2:
          parameterType: STRING
        rdc_result:
          parameterType: STRING
deploymentSpec:
  executors:
    exec-handle-collection-result:
      container:
        args:
        - --executor_input
        - '{{$}}'
        - --function_to_execute
        - handle_collection_result
        command:
        - python3
        - -m
        - kfp.dsl.executor_main
        image: image-registry.etri.re.kr:38090/nif-repo/handle_collection_result:v1.0.0
    exec-rd-collection:
      container:
        args:
        - --executor_input
        - '{{$}}'
        - --function_to_execute
        - rd_collection
        command:
        - python3
        - -m
        - kfp.dsl.executor_main
        image: image-registry.etri.re.kr:38090/nif-repo/rd_collection:v1.0.1
pipelineInfo:
  description: "\uD30C\uC77C\uACBD\uB85C\uB85C \uC9C0\uC815\uB41C \uC785\uB825 \uD30C\
    \uB77C\uBBF8\uD130\uB97C \uAE30\uBC18\uC73C\uB85C \uC218\uC9D1\uC8FC\uBB38 \uC694\
    \uCCAD\uC744 \uC2E4\uD589\uD558\uB294 \uCEF4\uD3EC\uB10C\uD2B8\uC640 \uADF8 \uACB0\
    \uACFC\uB97C \uCC98\uB9AC\uD558\uB294 \uD30C\uC774\uD504\uB77C\uC778 \uC608\uC2DC\
    .\nrd_collection \uCEF4\uD3EC\uB10C\uD2B8\uC640 handle_result \uCEF4\uD3EC\uB10C\
    \uD2B8 \uB450 \uAC1C\uB97C \uC5F0\uACB0\uD558\uC5EC \uD558\uB098\uC758 \uD30C\uC774\
    \uD504\uB77C\uC778\uC73C\uB85C \uAD6C\uC131\uD568.\nrd_collection \uCEF4\uD3EC\
    \uB10C\uD2B8\uB294 YAML \uD615\uC2DD\uC758 \uC785\uB825 \uD30C\uC77C\uC5D0 \uC788\
    \uB294 \uAC12\uC744 \uC77D\uC5B4\uC11C ODMC \uB85C \uC218\uC9D1\uC8FC\uBB38 \uC694\
    \uCCAD\uC744 \uC218\uD589\uD558\uACE0\n\uADF8 \uACB0\uACFC\uB97C JSON \uD615\uD0DC\
    \uC758 \uCD9C\uB825 \uD30C\uC77C\uC744 \uB9CC\uB4EC."
  name: rd-collection-pipeline
root:
  dag:
    outputs:
      parameters:
        output1:
          valueFromParameter:
            outputParameterKey: output1
            producerSubtask: rd-collection
        output2:
          valueFromParameter:
            outputParameterKey: output2
            producerSubtask: rd-collection
        output3:
          valueFromParameter:
            outputParameterKey: rdc_result
            producerSubtask: rd-collection
        output4:
          valueFromParameter:
            outputParameterKey: output1
            producerSubtask: handle-collection-result
        output5:
          valueFromParameter:
            outputParameterKey: output2
            producerSubtask: handle-collection-result
    tasks:
      handle-collection-result:
        cachingOptions: {}
        componentRef:
          name: comp-handle-collection-result
        dependentTasks:
        - rd-collection
        inputs:
          parameters:
            input1:
              taskOutputParameter:
                outputParameterKey: output1
                producerTask: rd-collection
            input2:
              taskOutputParameter:
                outputParameterKey: output2
                producerTask: rd-collection
        taskInfo:
          name: handle-collection-result
      rd-collection:
        cachingOptions: {}
        componentRef:
          name: comp-rd-collection
        inputs:
          parameters:
            input1:
              componentInputParameter: pipeline_input1
            mnt_path:
              componentInputParameter: mnt_path
        taskInfo:
          name: rd-collection
  inputDefinitions:
    parameters:
      mnt_path:
        defaultValue: /data
        description: "persistent volume\uC5D0 \uB300\uD55C mount path"
        isOptional: true
        parameterType: STRING
      pipeline_input1:
        defaultValue: /data/input_specify_duration.yaml
        description: "YAML \uD615\uC2DD\uC758 \uB0B4\uC6A9\uC744 \uC800\uC7A5\uD55C\
          \ \uD30C\uC77C path. \uCCAB \uBC88\uC9F8 \uCEF4\uD3EC\uB10C\uD2B8\uC758\
          \ \uC785\uB825 \uD30C\uB77C\uBBF8\uD130 \uAC12\uC73C\uB85C \uB4E4\uC5B4\uAC10"
        isOptional: true
        parameterType: STRING
      pvc_name:
        defaultValue: nif-volume
        description: "persistent volume \uC774\uB984"
        isOptional: true
        parameterType: STRING
  outputDefinitions:
    parameters:
      output1:
        parameterType: STRING
      output2:
        parameterType: STRING
      output3:
        parameterType: STRING
      output4:
        parameterType: STRING
      output5:
        parameterType: STRING
schemaVersion: 2.1.0
sdkVersion: kfp-2.2.0
---
platforms:
  kubernetes:
    deploymentSpec:
      executors:
        exec-handle-collection-result:
          pvcMount:
          - componentInputParameter: pvc_name
            mountPath: /data
        exec-rd-collection:
          pvcMount:
          - componentInputParameter: pvc_name
            mountPath: /data
