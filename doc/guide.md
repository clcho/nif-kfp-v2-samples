# NIF 테스트베드에서 KFP SDK v2 기반 컴포넌트 및 파이프라인 개발, 설치, 실행하는 절차와 방법

Created: April 4, 2024 1:35 PM
Edited: May 20, 2024 6:03 PM

# Harbor Registry 사용을 위한 설정

- 주의: 본 과정은 sudoer 권한이 있어야 함. 필요할 경우 관리자에게 요청 필요
- Harbor 호스트 접속
    
    ```bash
    ssh -p 40300 user_name@harbor_host_ip
    ```
    
- 사용자 계정을 docker group에 등록
    - 주의: 본 과정은 sudoer 권한이 있어야 함. 필요할 경우 관리자에게 요청 필요
    - 이렇게 하면 sudo 없이 docker를 사용할 수 있음
    - 현재 harbor 호스트에는 현재 clcho, modutech 계정을 docker group에 사용자 등록함
    
    ```bash
    sudo usermod -aG docker $USER
    ```
    
    - 주의: 사용자 추가한 후에 해당 사용자 계정으로 다시 재접속해야 반영이 됨
- docker 명령어 시험
    - sudo 없이 잘 되는지 확인
    - 이 명령어가 제대로 실행되지 않는 경우 docker 그룹에 본인의 id가 등록되지 않은 경우이므로 관리자에게 등록 요청 필요
    
    ```python
    $ docker images
    REPOSITORY                                            TAG                IMAGE ID       CREATED         SIZE
    python                                                3.10.11-bullseye   df5a406e137e   10 months ago   911MB
    hello-world                                           latest             d2c94e258dcb   11 months ago   13.3kB
    image-registry.etri.re.kr/library/hello-world         latest             d2c94e258dcb   11 months ago   13.3kB
    goharbor/chartmuseum-photon                           v1.10.4            4d6611b3b6a9   3 years ago     178MB
    goharbor/harbor-migrator                              v1.10.4            c6ba18cc92c0   3 years ago     357MB
    goharbor/redis-photon                                 v1.10.4            1733199a8380   3 years ago     122MB
    goharbor/clair-adapter-photon                         v1.10.4            4d7fec33eb52   3 years ago     61.2MB
    goharbor/clair-photon                                 v1.10.4            48f8d69c3f63   3 years ago     171MB
    goharbor/notary-server-photon                         v1.10.4            3cc30fe05041   3 years ago     143MB
    goharbor/notary-signer-photon                         v1.10.4            46ecb328c811   3 years ago     140MB
    goharbor/harbor-registryctl                           v1.10.4            503dda3f193e   3 years ago     102MB
    goharbor/registry-photon                              v1.10.4            96183605aaeb   3 years ago     84.5MB
    goharbor/nginx-photon                                 v1.10.4            f8f638056eee   3 years ago     43.6MB
    goharbor/harbor-log                                   v1.10.4            b0de11e1ba03   3 years ago     82.1MB
    goharbor/harbor-jobservice                            v1.10.4            91c262f629d2   3 years ago     143MB
    goharbor/harbor-core                                  v1.10.4            cc013d5caa80   3 years ago     129MB
    goharbor/harbor-portal                                v1.10.4            fec0c21d0a67   3 years ago     51.7MB
    goharbor/harbor-db                                    v1.10.4            2f077a558a2c   3 years ago     161MB
    goharbor/prepare                                      v1.10.4            85d07a7c81cd   3 years ago     168MB
    ```
    
- 참고: harbor가 아닌 다른 호스트에서 작업할 경우 image registry의 certificate 복사 (harbor 서버에 사용자 계정이 있어야 함)
    
    ```bash
    sudo scp -P 40300 user_id@image-registry.etri.re.kr:/etc/docker/certs.d/image-registry.etri.re.kr:38090/ca.crt /etc/docker/certs.d/image-registry.etri.re.kr:38090/ca.crt
    ```
    
- harbor image-registry에 login
    - 한 번만 login하면 ~/.docker/config.json 파일이 만들어져서 그 다음부터 따로 login 할 필요가 없음
        - harbor user id, pw는 harbor web UI의 id, pw와 동일함 (관리자에게 문의)
    
    ```python
    $ docker login image-registry.etri.re.kr:38090
    login: [harbor user id]
    password: [harbor user pw]
    ```
    
- Harbor Web UI에서 nif-repo 프로젝트 생성
    - Projects 메뉴에서 “NEW PROJECT” 사용하여 `nif-repo` 이름으로 프로젝트를 생성
    - 이 과정은 최초에 한 번만 하면 됨
    - NIF 과제에서 만드는 이미지들은 이 프로젝트 디렉토리 아래에 생성하도록 함

# Python 가상 환경 관리 도구 설치

- harbor 호스트에 접속
- python 가상 환경 관리 도구 miniforge 설치
    - 주의: anaconda를 사용하지 말 것! 라이선스 비용 이슈 있음
    
    ```bash
    cd ~/Downloads
    wget "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh" --no-check-certificate
    bash Miniforge3-$(uname)-$(uname -m).sh
    
    ```
    - 다음 과정에서 yes를 입력하여 shell을 시작할 때 자동으로 conda를 활성화하도록 설정 (.bashrc 파일에 관련 스크립트가 추가됨.)
    ```
    Do you wish to update your shell profile to automatically initialize conda?
    This will activate conda on startup and change the command prompt when activated.
    If you'd prefer that conda's base environment not be activated on startup,
    run the following command when conda is activated:

    conda config --set auto_activate_base false

    You can undo this by running `conda init --reverse $SHELL`? [yes|no]
    [no] >>> yes
    ```

- base environment 활성화
    - 아래 예에서 conda 설치 디렉토리 부분을 알맞게 수정 후 실행
    
    ```bash
    eval "$(/home/clcho/miniforge3/bin/conda shell.bash hook)"
    ```
    
- 시작할 때 environment 자동 활성화 (옵션)
    
    ```bash
    conda config --set auto_activate_base true
    ```
    
- 시작할 때 environment 자동 활성화 취소 (옵션)
    
    ```bash
    conda config --set auto_activate_base false
    ```
    

# Python 가상 환경 구성

- 가상환경 생성
    - python version은 3.7 이상으로 선택
    
    ```bash
    conda create -n nif-kfp-v2-env python=3.7
    ```
    
- 가상환경 활성화
    
    ```bash
    conda activate nif-kfp-v2-env
    ```
    
- kfp python package 설치
    - 버전 2.2.0로 설치
        - 더 높은 버전도 가능할 것으로 보이나 시험해 보지 않았음 (문서 작성 현재 최신 버전은 2.7.0)
    
    ```bash
    pip install kfp[all]==2.2.0
    ```
    

# 베이스 이미지 제작

- 개요
    - 필요한 패키지가 포함된 베이스 이미지를 만드는 과정이다. (이미 만들어진 베이스 이미지를 사용할 경우에는 생략)
    - nif-kfp-v2-samples의 base_image 디렉토리 참조
- runtime-requirements.txt 편집
    - 설치할 파이썬 패키지 목록을 적는다.
        
        ```
        kfp[all]==2.2.0
        ```
        
- Dockerfile 편집
    
    ```
    FROM python:3.7
    RUN pip install --upgrade pip
    COPY runtime-requirements.txt runtime-requirements.txt
    RUN pip install --no-cache-dir -r runtime-requirements.txt
    
    ```
    
- build_image.sh 파일 편집
    
    ```
    #!/bin/bash -e
    image_name=image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0
    image_tag=v1.0.0
    full_image_name=${image_name}:${image_tag}
    
    cd "$(dirname "$0")"
    docker build --network=host --no-cache --force-rm -t "${full_image_name}" .
    docker push "$full_image_name"
    
    # Output the strict image name, which contains the sha256 image digest
    docker inspect --format="{{index .RepoDigests 0}}" "${full_image_name}"
    
    ```
    
- build_image.sh 실행
    - 참고: 이 이미지는 Harbor의 nif-repo 프로젝트 아래에 들어가도록 설정되어 있음. 사전에 미리 Harbor Web UI에서 nif-repo 프로젝트가 생성되어 있어야 함.
    
    ```
    $ chmod 755 build_image.sh 
    $ ./build_image.sh
    [+] Building 26.6s (9/9) FINISHED
     => [internal] load build definition from Dockerfile                                                                                                                                                                                                      0.1s
     => => transferring dockerfile: 199B                                                                                                                                                                                                                      0.0s
     => [internal] load metadata for docker.io/library/python:3.7                                                                                                                                                                                             0.0s
     => [internal] load .dockerignore                                                                                                                                                                                                                         0.1s
     => => transferring context: 2B                                                                                                                                                                                                                           0.0s
     => [1/4] FROM docker.io/library/python:3.7                                                                                                                                                                                                               0.3s
     => [internal] load build context                                                                                                                                                                                                                         0.1s
     => => transferring context: 66B                                                                                                                                                                                                                          0.0s
     => [2/4] RUN pip install --upgrade pip                                                                                                                                                                                                                   4.6s
     => [3/4] COPY runtime-requirements.txt runtime-requirements.txt                                                                                                                                                                                          0.1s
     => [4/4] RUN pip install --no-cache-dir -r runtime-requirements.txt                                                                                                                                                                                     20.2s
     => exporting to image                                                                                                                                                                                                                                    1.1s
     => => exporting layers                                                                                                                                                                                                                                   1.0s
     => => writing image sha256:7d77f6c7113ffd5fd7f769a2c5402355bb8a3eee5f984070476973e398feb7fc                                                                                                                                                              0.0s
     => => naming to image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0:v1.0.0                                                                                                                                                                       0.0s
    The push refers to repository [image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0]
    07f520b1c3a7: Pushed
    a5a55cbcf46a: Pushed
    982184d81722: Pushed
    45c430b35dba: Mounted from nif-repo/handle_collection_result
    8e23f007f16f: Mounted from nif-repo/handle_collection_result
    aef22e07d5d7: Mounted from nif-repo/handle_collection_result
    c26432533a6a: Mounted from nif-repo/handle_collection_result
    01d6cdeac539: Mounted from nif-repo/handle_collection_result
    a981dddd4c65: Mounted from nif-repo/handle_collection_result
    f6589095d5b5: Mounted from nif-repo/handle_collection_result
    7c85cfa30cb1: Mounted from nif-repo/handle_collection_result
    v1.0.0: digest: sha256:a7149574a12ff3ef4d45cd06fdc67a4960315ee5062257bcca068cbe32e11ea4 size: 2636
    image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0@sha256:a7149574a12ff3ef4d45cd06fdc67a4960315ee5062257bcca068cbe32e11ea4
    
    ```
    
- local registry에 이미지 등록 확인
    
    ```
    $ docker images
    REPOSITORY                                                    TAG                IMAGE ID       CREATED          SIZE
    image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0   v1.0.0             d37f4a2759cd   6 minutes ago    1.06GB
    ```
    
- Harbor Web UI에서 이미지 등록 확인
    - ETRI 내부에서 확인 방법
        - Harbor Web UI의 URL 및 ID, PW는 관리자에게 문의
        - Projects → nif-repo 선택 → Repositories 선택 → nif-repo/python3.7_kfp2.2.0 선택 → 버전 확인
            
            ![Untitled](images/Untitled.png)
            
        - 오래되어 사용하지 않는 버전은 관리자에게 요청하여 삭제할 것
    - ETRI 외부에서 확인 방법
        - harbor 서버 터미널에서 다음 명령어를 실행하여 이미지가 제대로 harbor registry에 push되어 들어갔는지 확인.
            - 해당 repository의 tag 목록에 새로 push한 tag가 보이는지 확인
                
                ```bash
                MY_REPO_NAME="nif-repo/python3.7_kfp2.2.0"
                curl -X GET "https://image-registry.etri.re.kr:38090/api/repositories/$MY_REPO_NAME/tags" -H "accept: application/json" --insecure 
                ```
                
        - 오래되어 사용하지 않는 버전은 관리자에게 요청하여 삭제할 것

# 샘플 컴포넌트 제작

- nif-kfp-v2-samples의 components 디렉토리 참조
- 컴포넌트 python 코딩
    - 참고 자료
        - Data types and parameters
            - [https://www.kubeflow.org/docs/components/pipelines/v2/data-types/parameters/](https://www.kubeflow.org/docs/components/pipelines/v2/data-types/parameters/)
        - Container Components
            - [https://www.kubeflow.org/docs/components/pipelines/v2/components/container-components/](https://www.kubeflow.org/docs/components/pipelines/v2/components/container-components/)
        - Platform-specific Features
            - [https://www.kubeflow.org/docs/components/pipelines/v2/platform-specific-features/](https://www.kubeflow.org/docs/components/pipelines/v2/platform-specific-features/)
        - Load and Share Components
            - [https://www.kubeflow.org/docs/components/pipelines/v2/load-and-share-components/](https://www.kubeflow.org/docs/components/pipelines/v2/load-and-share-components/)
        - Compile a Pipeline
            - [https://www.kubeflow.org/docs/components/pipelines/v2/compile-a-pipeline/](https://www.kubeflow.org/docs/components/pipelines/v2/compile-a-pipeline/)
        - Kubeflow Pipelines SDK API Reference
            - [https://kubeflow-pipelines.readthedocs.io/en/stable/](https://kubeflow-pipelines.readthedocs.io/en/stable/)
    - 소스 구조
        - nif-kfp-v2-samples/components/sample_component1/src/ 디렉토리 아래에 python 소스 코드들을 작성함
            
            ```bash
            nif-kfp-v2-samples/components/sample_component1/src/sample_component1.py
            ```
            
    - sample_component1.py
        - `@dsl.component` 데코레이터를 사용하여 base_image와 target_image 이름 지정
        
        ```python
        from kfp import dsl
        import json
        from typing import NamedTuple
        import uuid
        
        @dsl.component(base_image='image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0:v1.0.0',
                       target_image='image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.5')
        def sample_component1(input1: str, input2: int, mnt_path: str) -> NamedTuple('outputs', output1=str, output2=int):
            """
            JSON 형식으로 된 input1 파일에서 수를 읽어서 input2의 값과 더하여 그 결과 값을 동일한 JSON 형식으로 output1 파일에 저장한다.
            output2에는 결과 값을 저장한다.
            input1과 output1 파일의 형식은 다음과 같다.
             {"input": 231}
            :param input1: input file path
            :param input2: integer
            :param mnt_path: persistent volume의 mount path
            :return: NamedTuple('outputs', output1=str, output2=int)
              output1: output file path
              output2: integer
            """
        
            output_parent = mnt_path
            output_path = output_parent + f'/output-{uuid.uuid4()}.json'
        
            with open(input1, 'r') as input1_file:
                data = json.load(input1_file)
            data['input'] = data['input'] + input2
            output2 = data['input']
        
            with open(output_path, 'w') as o_file:
                json.dump(data, o_file)
        
            outputs = NamedTuple('outputs', output1=str, output2=int)
            return outputs(output_path, output2)
        
        ```
        
    - 코드를 수정할 때마다 target_image의 버전 번호를 업데이트 할 것.
        - 버전 번호를 바꾸지 않고 이미지를 push했을 때 제대로 반영이 안되는 경우가 있었음. 정확한 원인은 모름.
- kfp component 빌드하기
    - nif-kfp-v2-samples/components/sample_component1 디렉토리에서 실행
        
        ```bash
        kfp component build src/ --component-filepattern sample_component1.py --overwrite-dockerfile --push-image
        
        ```
        
    - 실행 예
        
        ```bash
        $ kfp component build src/ --component-filepattern sample_component1.py --overwrite-dockerfile --push-image
        Building component using KFP package path: kfp==2.2.0
        Found 1 component(s) in file /home/clcho/nif-kfp-v2-samples/components/sample_component1/src/sample_component1.py:
        Sample component1: ComponentInfo(name='Sample component1', function_name='sample_component1', func=<function sample_component1 at 0x7fae325b1d40>, target_image='image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.5', module_path=PosixPath('/home/clcho/nif-kfp-v2-samples/components/sample_component1/src/sample_component1.py'), component_spec=ComponentSpec(name='sample-component1', implementation=Implementation(container=ContainerSpecImplementation(image='image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.5', command=['python3', '-m', 'kfp.components.executor_main'], args=['--executor_input', '{{$}}', '--function_to_execute', 'sample_component1'], env=None, resources=None), importer=None, graph=None), description='JSON 형식으로 된 input1 파일에서 수를 읽어서 input2의 값과 더하여 그 결과 값을 동일한 JSON 형식으로 output1 파일에 저장한다.\noutput2에는 결과 값을 저장한다.\ninput1과 output1 파일의 형식은 다음과 같다.\n {"input": 231}', inputs={'input1': InputSpec(type='String', default=None, optional=False, is_artifact_list=False, description='input file path'), 'input2': InputSpec(type='Integer', default=None, optional=False, is_artifact_list=False, description='integer'), 'mnt_path': InputSpec(type='String', default=None, optional=False, is_artifact_list=False, description='persistent volume의 mount path')}, outputs={'output1': OutputSpec(type='String', is_artifact_list=False, description=None), 'output2': OutputSpec(type='Integer', is_artifact_list=False, description=None)}, platform_spec=), output_component_file=None, base_image='image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0:v1.0.0', packages_to_install=[], pip_index_urls=None)
        Using base image: image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0:v1.0.0
        Using target image: image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.5
        Found existing file runtime-requirements.txt under /home/clcho/nif-kfp-v2-samples/components/sample_component1/src.
        Overwriting existing file runtime-requirements.txt
        Generated file /home/clcho/nif-kfp-v2-samples/components/sample_component1/src/runtime-requirements.txt.
        Found existing file .dockerignore under /home/clcho/nif-kfp-v2-samples/components/sample_component1/src.
        Leaving this file untouched.
        Found existing file Dockerfile under /home/clcho/nif-kfp-v2-samples/components/sample_component1/src.
        Overwriting existing file Dockerfile
        Generated file /home/clcho/nif-kfp-v2-samples/components/sample_component1/src/Dockerfile.
        Building image image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.5 using Docker...
        Docker: Step 1/6 : FROM image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0:v1.0.0
        Docker:  ---> d37f4a2759cd
        Docker: Step 2/6 : WORKDIR /usr/local/src/kfp/components
        Docker:  ---> Using cache
        Docker:  ---> 7ab477c7de10
        Docker: Step 3/6 : COPY runtime-requirements.txt runtime-requirements.txt
        Docker:  ---> Using cache
        Docker:  ---> 97cb2fd5e6ef
        Docker: Step 4/6 : RUN pip install --no-cache-dir -r runtime-requirements.txt
        Docker:  ---> Using cache
        Docker:  ---> de1feec31f27
        Docker: Step 5/6 : RUN pip install --no-cache-dir kfp==2.2.0
        Docker:  ---> Using cache
        Docker:  ---> 836cbc771c18
        Docker: Step 6/6 : COPY . .
        Docker:  ---> Using cache
        Docker:  ---> 6f956de99fa9
        Docker: Successfully built 6f956de99fa9
        Docker: Successfully tagged image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.5
        Pushing image image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.5...
        Docker:  The push refers to repository [image-registry.etri.re.kr:38090/nif-repo/sample_component1]
        Docker: 73cfbf611f02 Preparing
        Docker: 6c0d269698b2 Preparing
        
        ...
        
        Docker: 7c85cfa30cb1 Layer already exists
        Docker: a981dddd4c65 Layer already exists
        Docker:  v1.0.5: digest: sha256:cc7226580bf12e0e312c877cd28b5623f4c29b9e25f73ff12501ab43f374652d size: 3671
        Built and pushed component container image-registry.etri.re.kr:38090/nif-repo/sample_component1:v1.0.5
        
        ```
        
        - 마지막 라인에서 `Built and pushed component container …` 확인할 것. 이 라인이 없으면 빌드가 실패한 것임.
        - 참고: 이 이미지는 Harbor의 nif-repo 프로젝트 아래에 들어가도록 설정되어 있음. 사전에 미리 Harbor Web UI에서 nif-repo 프로젝트가 생성되어 있어야 함.
- local registry에 이미지 등록 확인
    
    ```bash
    $ docker images
    REPOSITORY                                                    TAG                IMAGE ID       CREATED             SIZE
    image-registry.etri.re.kr:38090/nif-repo/sample_component1    v1.0.5             6f956de99fa9   46 minutes ago      1.06GB
    
    ```
    
- Harbor Web UI에서 이미지 등록 확인
    - ETRI 내부에서 확인 방법
        - Harbor Web UI의 URL 및 ID, PW는 관리자에게 문의
        - Projects → nif-repo 선택 → Repositories 선택 → nif-repo/sample_component1 선택 → 버전 확인
            
            ![Untitled](images/Untitled%201.png)
            
        - 오래되어 사용하지 않는 버전은 관리자에게 요청하여 삭제할 것
    - ETRI 외부에서 확인 방법
        - harbor 서버 터미널에서 다음 명령어를 실행하여 이미지가 제대로 harbor registry에 push되어 들어갔는지 확인.
            - 해당 repository의 tag 목록에 새로 push한 tag가 보이는지 확인
                
                ```bash
                MY_REPO_NAME="nif-repo/sample_component1"
                curl -X GET "https://image-registry.etri.re.kr:38090/api/repositories/$MY_REPO_NAME/tags" -H "accept: application/json" --insecure 
                ```
                
        - 오래되어 사용하지 않는 버전은 관리자에게 요청하여 삭제할 것
- 생성된 component yaml 파일 확인
    - src/component_metadata/ 디렉토리에 생성됨
    - pipeline을 정의할 때 이렇게 생성된 component yaml 파일을 사용하게 됨
    
    ```bash
    $ cat src/component_metadata/sample_component1.yaml
    
    ```
    

# 샘플 파이프라인 제작

- nif-kfp-v2-samples의 pipelines 디렉토리 참조
- 파이프라인 제작에 사용할 component yaml 파일 획득
    - 이 예제에서는 사용할 component yaml 파일이 아래 디렉토리에 있다고 가정함
        - `../components/sample_component1/src/component_metadata/sample_component1.yaml`
- 파이프라인 python 코드 작성
    - 소스 구조
        - nif-kfp-v2-samples/pipelines/ 디렉토리 아래에서 pipeline 코드를 작성함
    - sample_pipeline1.py
        
        ```python
        from kfp import dsl
        from kfp import components
        from kfp import compiler
        from typing import NamedTuple
        from kfp import kubernetes
        
        # pipeline에서 사용할 component yaml 파일을 읽어서 component 객체를 생성함
        sample_comp1 = components.load_component_from_file(
            '../components/sample_component1/src/component_metadata/sample_component1.yaml')
        
        @dsl.pipeline
        def sample_pipeline_1(
                pipeline_input1: str="/data/sample1_input.json",
                pipeline_input2: int=1,
                pvc_name: str="clcho-volume",
                mnt_path: str="/data") -> NamedTuple('pipeline_outputs', output1=str, output2=str, output3=int, output4=int):
            """
            파일 path로 지정된 파라미터를 처리하는 방법을 보여주는 샘플 파이프라인.
            sample_component1 컴포넌트 두 개를 연결하여 하나의 파이프라인으로 구성함.
            sample_component1 컴포넌트는 JSON 형식의 입력 파일에 있는 값을 읽어서 두 번째 입력 파라미터 값과 더하여 출력 파일을 만듬.
        
            :param pipeline_input1: JSON 형식의 내용을 저장한 파일 path. 첫 번째 컴포넌트의 입력 파라미터 값으로 들어감
            :param pipeline_input2: integer
            :param pvc_name: persistent volume 이름
            :return:
            """
            task1 = sample_comp1(input1=pipeline_input1, input2=pipeline_input2, mnt_path=mnt_path)
            task2 = sample_comp1(input1=task1.outputs['output1'], input2=pipeline_input2, mnt_path=mnt_path)
        
            # 아래에서 mount_path 값으로 파이프라인 파라미터로 받은 mnt_path를 사용하면 에러 발생함.
            # 아직 파이프라인 입력 파라미터로 받은 mnt_path를 사용하여 아래 mount_path를 지정하는 방법을 모름.
            # 이 이슈가 해결되기 전까지는 파이프라인 런을 생성할 때 mnt_path는 "/data" 값을 그대로 사용할 것.
            # kubernetes.mount_pvc(task1, pvc_name=pvc_name, mount_path=mnt_path)
            # kubernetes.mount_pvc(task2, pvc_name=pvc_name, mount_path=mnt_path)
            kubernetes.mount_pvc(task1, pvc_name=pvc_name, mount_path='/data')
            kubernetes.mount_pvc(task2, pvc_name=pvc_name, mount_path='/data')
            task2.after(task1)
        
            p_outputs = NamedTuple('pipeline_outputs', output1=str, output2=str, output3=int, output4=int)
        
            return p_outputs(
                task1.outputs['output1'],
                task2.outputs['output1'],
                task1.outputs['output2'],
                task2.outputs['output2'])
        
        compiler.Compiler().compile(sample_pipeline_1, 'sample_pipeline1.yaml')
        
        ```
        
- 파이프라인 컴파일
    - pipeline yaml 파일 생성
        
        ```bash
        python sample_pipeline1.py
        ```
        
    - pipeline yaml 파일 확인
        
        ```bash
        cat sample_pipeline1.yaml
        ```
        

# 파이프라인 시험

### Kubeflow Web UI 접속

- URL 및 ID, PW는 관리자에게 문의

### Persistent Volume 생성

- Kubeflow Web UI의 Volumes 메뉴에서 `+ New Volume` 선택
- Access Mode는 ReadWriteMany 선택
    
    ![Untitled](images/Untitled%202.png)
    

### Notebook 생성 및 파이프라인 입력 파일 생성

- Kubeflow Web UI의 Notebooks 메뉴에서 `+ New Notebook` 선택
- Notebook 이름 지정 (예: sample-notebook)
- Data Volume 추가: `+ Attach existing volume` 선택
- Name에서 sample-volume 선택
- Mount path로 `/home/jovyan/data` 입력
    
    ![Untitled](images/Untitled%203.png)
    
- `LAUNCH` 눌러서 notebook 생성
- 생성한 Notebook에서 `CONNECT`  클릭
- sample-notebook lab 화면의 오른쪽 파일 폴더에서 data 클릭
    
    ![Untitled](images/Untitled%204.png)
    
- File → New → Text File 선택
- 이름 변경: sample1_input.json
- 파일 내용으로 다음 형식으로 입력
    
    ```json
    {"input": 100}
    ```
    
- 터미널에서 파일 내용 확인
    
    ```json
    (base) jovyan@sample-notebook-0:~$ cd data
    (base) jovyan@sample-notebook-0:~/data$ cat sample1_input.json 
    {"input": 100}(base) jovyan@sample-notebook-0:~/data$ 
    ```
    

### Experiment 생성

- Kubeflow Web UI의 Experiments (KFP) 메뉴에서 `+ Create experiment` 선택
- experiment 이름 입력: 예) sample-experiment
- Next 버튼 누르면 experiment 생성됨 (여기에서 바로 run을 시작하지 않아도 됨)

### 파이프라인 업로드

- Kubeflow Web UI의 Pipelines 메뉴에서 `+ Upload pipeline` 선택
- Pipeline 이름 입력
- Upload a file 선택하여 샘플 파이프라인 제작에서 컴파일하여 만든 sample_pipeline1.yaml 파일 업로드
- `Create` 버튼 클릭

### 런 생성 및 실행

- 생성된 파이프라인에서 `+ Create run` 선택
- Experiment 선택: 미리 생성한 sample-experiment 선택 후 `Use this experiment` 버튼 클릭
- Run parameters 입력
    - 현재 default로 보이는 것 그대로 사용하면 됨
    - 수정할 경우 mnt_path는 pipeline 코드에서 사용하는 mount_path와 일치시켜야 함
    - pipeline_input1의 파일 path의 앞부분은 mnt_path와 일치시켜야 함
    - pvc_name은 앞서 생성한 persistent volume과 일치시켜야 함
        
        ![Untitled](images/Untitled%205.png)
        
- `Start` 버튼을 누르면 run이 시작됨
- Run의 Graph 화면에서 시간이 지나면 컴포넌트의 상태가 업데이트 됨
- Graph 화면에서 특정 컴포넌트를 선택하면 정보 화면이 나옴
    - Input/Output: 입출력 파라미터 값을 보여줌. Output의 경우 실행이 끝나야 보여줌
    - Task Details: 실행 상태 정보를 보여줌
    - Logs: 컴포넌트 실행이 끝나야 pod의 log를 보여줌
- 컴포넌트의 실행이 정상적으로 종료된 경우 Graph 화면에서 컴포넌트의 옆에 체크 표시가 되며, 비정상적으로 종료한 경우 느낌표 표시가 됨
    
    ![Untitled](images/Untitled%206.png)
    
- 각 컴포넌트를 선택하여 Input/Output 및 Logs 확인
    
    ![Untitled](images/Untitled%207.png)