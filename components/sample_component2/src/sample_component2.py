from kfp import dsl
import json
from typing import NamedTuple


@dsl.component(base_image='image-registry.etri.re.kr:38090/nif-repo/python3.7_kfp2.2.0:v1.0.0',
               target_image='image-registry.etri.re.kr:38090/nif-repo/sample_component2:v1.0.1')
def sample_component2(input1: int, input2: int) -> NamedTuple('outputs', output1=str, output2=int):
    """
    input1 값과 input2 값을 읽어서 두 수를 더하는 식을 str 형식으로 만들어서 output1으로 만든다.
    두 수의 합을 구하여 output2를 만든다.

    :param input1: number 1
    :param input2: number 2
    :return: NamedTuple('outputs', output1=str, output2=int)
    """

    output2 = input1 + input2
    output1 = f'{input1} + {input2} = {output2}'
    outputs = NamedTuple('outputs', output1=str, output2=int)
    return outputs(output1, output2)
