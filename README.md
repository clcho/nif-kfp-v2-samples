# NIF KFP v2 Samples

## Name
NIF KFP v2 Samples project

## Description
NIF 테스트베드에서 KFP SDK v2 기반 컴포넌트와 파이프라인 제작 및 실행하는 예제

## KFP SDK v2 기반 컴포넌트 및 파이프라인 개발, 설치, 시험 방법 

- [NIF 테스트베드에서 KFP SDK v2 기반 컴포넌트 및 파이프라인 개발, 설치, 시험하기 절차와 방법](doc/guide.md)
- [NIF KFP Recurring Runs를 사용하기 위한 주의 사항과 실행 방법](doc/recurring_runs.md)
- [NIF 데이터 수집 및 사전 처리 파이프라인 샘플](doc/rdc_repss_pipeline_sample.md)
