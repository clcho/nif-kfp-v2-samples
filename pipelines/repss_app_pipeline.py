from kfp import dsl
from kfp import components
from kfp import compiler
from kfp import kubernetes
from typing import NamedTuple

# pipeline에서 사용할 component yaml 파일을 읽어서 component 객체를 생성함
# repss_app_component = components.load_component_from_file("../components/repss-app-component/src/component_metadata/repss_component.yaml")
repss_app_component = components.load_component_from_file("../components/nif/repss_component.yaml")

# Define a pipeline that uses the OpenAPI app component
@dsl.pipeline(
    name="NIF OpenAPI Pipeline",
    description="A pipeline that uses the NIF OpenAPI component"
)
def repss_app_pipeline(
    # orderid: str = "430b8f2c-076f-4aa1-8c73-4d7f8b312b56",  # for testbed2
    # dataids: str = "[\"5a3e682d-3f41-4af6-b026-509a9010fced\"]",  # for testbed2
    orderid: str = "b3760387-a4c0-4803-9f5b-cffb0834ecc4",  # for testbed1
    dataids: str = "[\"4625f30e-68c9-4f45-87d9-be014cbe1f3c\",\"c7b1be8c-d3de-4356-9258-ac3b7925a2ca\",\"0d3978d4-fcb7-4666-b85e-cc6ca9dabe63\"]",  # for testbed1
    # url: str = "http://129.254.190.118:21050/repss/v2.0.0/orders", for testbed1
    url: str = "http://10.3.30.27:18090/repss/v2.0.0/orders",
    method: str = "POST"
) -> NamedTuple('pipeline_outputs', result=str):

    task1 = repss_app_component(orderid=orderid, dataids=dataids, url=url, method=method)
    task1.set_caching_options(False)

    p_outputs = NamedTuple('pipeline_outputs', result=str)
    
    return p_outputs(task1.outputs['output'])

compiler.Compiler().compile(repss_app_pipeline, "repss_app_pipeline.yaml")
