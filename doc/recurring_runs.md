# NIF KFP Recurring Runs를 사용하기 위한 주의 사항과 실행 방법

Created: April 29, 2024 6:02 PM
Edited: April 30, 2024 8:50 AM

## Periodic Trigger Type의 Recurring Run 사용 방법

### Periodic Trigger Type의 Recurring Run을 사용하기 위한 주의 사항

- task의 caching option을  False로 설정할 것
    - pipeline code에서 component task의 caching option을 False로 설정해야 함
        
        ```python
        task1.set_caching_options(False)
        ```
        
    - default 값이 True여서 task를 반복 실행할 때 task의 입력에 변화가 없으면 cache에서 바로 출력값을 가져옴 (즉, 실제 실행하지 않음)
- `maximum concurrent runs` 옵션은 1로 설정할 것
    - 이 옵션은 동시에 실행할 수 있는 run의 갯수를 의미함. 한 번의 run이 지정한 period보다 오래 실행될 경우 그 다음 period가 시작될 때 새로운 run을 최대 몇 개까지 실행할지를 지정하는 것임.
- `catchup` 옵션은 off로 설정할 것
    - Recurring Run을 중단하는 등으로 run의 실행이 미루어진 경우 이를 나중에라도 실행할지를 지정하는 옵션
- `has start date`와 `has end date` 옵션을 지정할 것
    - 특히 `has end date` 옵션을 지정하지 않으면 Recurring Run을 disable 시킬 때까지 무한히 실행됨
- `has start date` 옵션을 지정한 경우 지정한 그 시간에 첫 번째 run이 시작하지 않음
    - 시작 시간(start date) + period 후에 첫 번째 run을 시작함
    - 예 1: recurring run으로 period 10분, 시작 시간 9시 30분, 종료 시간 10시 0분으로 지정한 경우
        - 첫 번째 run은 9시 40분에 시작
        - 두 번째 run은 9시 50분에 시작
        - 세 번째 run은 10시 0분에 시작 (period 도래 시간 ≤ 종료 시간이면 새로운 run 시작함)
    - 예 2: recurring run으로 period 20분, 시작 시간 10시 35분, 종료 시간 11시 40분으로 지정한 경우
        - 첫 번째 run은 10시 55분에 시작
        - 두 번째 run은 11시 15분에 시작
        - 세 번째 run은 11시 35분에 시작
    - **주목: 시작 시간은 이미 지나간 시간이어도 됨**
        - 만일 현재 시간이 2시 50분이며, 3시부터 시작해서 한 시간 주기로 6시까지 Recurring Run을 실행하고 싶다면 시작 시간을 2시 0분으로 설정하고 종료 시간을 6시 0분으로 설정하면 됨
- Recurring Run은 종료 시간(end date) 이후에도 계속 enabled 상태로 남아 있음
    - Kubeflow Recurring Runs 목록에서 Recurring Run을 선택하여 직접 disable 시켜주어야 함

### Periodic Trigger Type의 Recurring Run 실행 방법

- Kubeflow Web UI의 Pipelines 메뉴에서 run을 생성할 파이프라인 선택
    
    ![Untitled](recurring_runs_images/Untitled.png)
    
- Pipeline 화면에서 “+ Create run” 버튼 클릭
- “Start a new run” 화면에서 가장 먼저 experiment 선택 (나중에 experiment를 선택하면 이전에 입력했던 내용이 리셋됨)
- `Run Type`으로 Recurring 선택
- `Trigger type` 으로 Periodic 선택
- `Maximum concurrent runs` 항목은 1로 설정
- `Has start date` 체크 박스 선택 후 시작 시간 설정
- `Has end date` 체크 박스 선택 후 종료 시간 설정
- `Catchup` 체크 박스 해제
- Run every 항목 설정
- Run parameters 항목 설정
- “Start” 버튼 클릭
    
    ![Untitled](recurring_runs_images/Untitled%201.png)
    

## Cron Trigger Type의 Recurring Run 사용 방법

### Cron Trigger Type의 Recurring Run을 사용하기 위한 주의 사항

- task의 caching option을  False로 설정할 것
    - pipeline code에서 component task의 caching option을 False로 설정해야 함
        
        ```python
        task1.set_caching_options(False)
        ```
        
    - default 값이 True여서 task를 반복 실행할 때 task의 입력에 변화가 없으면 cache에서 바로 출력값을 가져옴 (즉, 실제 실행하지 않음)
- `maximum concurrent runs` 옵션은 1로 설정할 것
    - 이 옵션은 동시에 실행할 수 있는 run의 갯수를 의미함. 한 번의 run이 지정한 period보다 오래 실행될 경우 그 다음 period가 시작될 때 새로운 run을 최대 몇 개까지 실행할지를 지정하는 것임.
- `catchup` 옵션은 off로 설정할 것
    - Recurring Run을 중단하는 등으로 run의 실행이 미루어진 경우 이를 나중에라도 실행할지를 지정하는 옵션
- `has start date`와 `has end date` 옵션을 지정할 것
    - 특히 `has end date` 옵션을 지정하지 않으면 Recurring Run을 disable 시킬 때까지 무한히 실행됨
- `has start date` 옵션을 지정한 경우 지정한 그 시간 이후에 cron에 의해 지정된 시간마다 run이 실행됨
- Recurring Run은 종료 시간(end date) 이후에도 계속 enabled 상태로 남아 있음
    - Kubeflow Recurring Runs 목록에서 Recurring Run을 선택하여 직접 disable 시켜주어야 함
- cron expression의 의미
    - 공백으로 구분되는 6개의 필드로 구성됨
    - 각 필드의 의미는 “초, 분, 시, 일, 월, 요일”
    - cron expression format은 다음 자료 참고
        - [https://pkg.go.dev/github.com/robfig/cron#hdr-CRON_Expression_Format](https://pkg.go.dev/github.com/robfig/cron#hdr-CRON_Expression_Format)

### Cron Trigger Type의 Recurring Run 실행 방법

- Kubeflow Web UI의 Pipelines 메뉴에서 run을 생성할 파이프라인 선택
- Pipeline 화면에서 “+ Create run” 버튼 클릭
- “Start a new run” 화면에서 가장 먼저 experiment 선택 (나중에 experiment를 선택하면 이전에 입력했던 내용이 리셋됨)
- `Run Type`으로 Recurring 선택
- `Trigger type` 으로 Cron 선택
- `Maximum concurrent runs` 항목은 1로 설정
- `Has start date` 체크 박스 선택 후 시작 시간 설정 (옵션)
- `Has end date` 체크 박스 선택 후 종료 시간 설정
- `Catchup` 체크 박스 해제
- `Run every` 항목 설정하거나 `Allow editing cron expression`을 선택하여 cron expression 편집
    - `Run every` 항목을 설정하면 그에 따라 cron expression이 자동으로 변경됨
    - `Run every` 항목 설정만으로 표현할 수 없을 경우 `Allow editing cron expression`을 선택하여 cron expression을 직접 편집할 수 있음
        - 예 1: 매 10분마다 run 실행
            
            ```python
            0 0,10,20,30,40,50 * * * ?
            ```
            
        - 예 2: 매일 오전 2시, 오전 10시, 오후 4시, 오후 10시에 run 실행
            
            ```python
            0 0 2,10,16,22 * * ?
            ```
            
        - 예 3: 매주 월요일과 목요일 자정에만 run 실행
            
            ```python
            0 0 0 * * 1,4
            ```
            
- Run parameters 항목 설정
- “Start” 버튼 클릭
    
    ![Untitled](recurring_runs_images/Untitled%202.png)
    

## Recurring Run 실행 상태 및 결과 확인

- Kubeflow Web UI의 Recurring Runs 메뉴에서 Recurring Runs 목록 확인
    
    ![Untitled](recurring_runs_images/Untitled%203.png)
    
- Recurring Runs 설정에 따른 실제 실행된 run은 Runs 메뉴에서 확인
    
    ![Untitled](recurring_runs_images/Untitled%204.png)