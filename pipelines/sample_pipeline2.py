from kfp import dsl
from kfp import components
from kfp import compiler
import json
from typing import NamedTuple


# pipeline에서 사용할 component yaml 파일을 읽어서 component 객체를 생성함
sample_comp2 = components.load_component_from_file(
    '../components/sample_component2/src/component_metadata/sample_component2.yaml')

@dsl.pipeline
def sample_pipeline_2(
        pipeline_input1: int,
        pipeline_input2: int,
        pipeline_input3: int) -> NamedTuple('pipeline_outputs', output1=str, output2=str):

    task1 = sample_comp2(input1=pipeline_input1, input2=pipeline_input2)
    task2 = sample_comp2(input1=task1.outputs['output2'], input2=pipeline_input3)

    p_outputs = NamedTuple('pipeline_outputs', output1=str, output2=int)

    return p_outputs(task1.outputs['output1'], task2.outputs['output1'])


compiler.Compiler().compile(sample_pipeline_2, 'sample_pipeline2.yaml')
